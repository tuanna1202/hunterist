package com.devdn.ctc.hunterist.ui.fragment;

import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

/**
 * Base Fragment.
 *
 * @author CuongNV
 */
@EFragment
public abstract class BaseFragment extends Fragment {

    @AfterViews
    protected abstract void init();

    protected void showAlertDialog(@NonNull String msg) {
        new AlertDialog.Builder(getContext())
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    protected void showAlertDialog(@StringRes int resId) {
        showAlertDialog(getString(resId));
    }
}
