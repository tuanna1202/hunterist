package com.devdn.ctc.hunterist.utils.widget;

import android.content.Context;
import android.graphics.drawable.Animatable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.devdn.ctc.hunterist.R;

/**
 * Custom Loading Circle.
 *
 * @author CuongNV
 */
public class LoadingCircle extends ImageView {
    public LoadingCircle(Context context) {
        this(context, null);
    }

    public LoadingCircle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        this.setImageResource(R.drawable.general_loading);
        ((Animatable) this.getDrawable()).start();
    }
}
