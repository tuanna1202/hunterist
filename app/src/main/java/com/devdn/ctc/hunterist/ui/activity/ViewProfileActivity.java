package com.devdn.ctc.hunterist.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.adapter.ViewProfileAdapter;
import com.devdn.ctc.hunterist.utils.widget.HeaderBar;
import com.devdn.ctc.hunterist.utils.widget.HidingScrollListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by cuongnv on 09/11/15.
 */
@EActivity(R.layout.activity_view_profile)
public class ViewProfileActivity extends BaseActivity {
    /**
     * Main Adapter
     */
    private ViewProfileAdapter mAdapter;
    @ViewById
    RecyclerView mRecyclerViewMain;
    @ViewById
    HeaderBar mHeaderBar;
    @ViewById
    ImageView mImgReporterBg;
    @ViewById
    ImageView mImgReporterBgMask;

    @Override
    protected void init() {
        initBackground();
        mAdapter = new ViewProfileAdapter(this);
        mRecyclerViewMain.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewMain.setHasFixedSize(true);
        mRecyclerViewMain.setAdapter(mAdapter);
        mRecyclerViewMain.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    private void initBackground() {
        Picasso.with(this)
                .load(R.drawable.bg_1)
                .fit()
                .centerCrop()
                .into(mImgReporterBg, new Callback() {
                    @Override
                    public void onSuccess() {
                        Picasso.with(ViewProfileActivity.this)
                                .load(R.drawable.bg_1)
                                .fit()
                                .centerCrop()
                                .into(mImgReporterBgMask);
                    }

                    @Override
                    public void onError() {
                    }
                });
    }

    @Click(R.id.mBtnFloatingHome)
    void onFloatingHomeClick() {
        Toast.makeText(this, "create new trip", Toast.LENGTH_SHORT).show();
    }

    private void hideViews() {
        mHeaderBar.setTitle("CuongNV");
    }

    private void showViews() {
        mHeaderBar.setTitle("");
        mHeaderBar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }
}
