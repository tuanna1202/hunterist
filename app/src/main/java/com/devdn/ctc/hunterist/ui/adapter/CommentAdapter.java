package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.devdn.ctc.hunterist.R;

/**
 * Created by chien on 19/11/2015.
 */
public class CommentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;

    public CommentAdapter(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        final View view = LayoutInflater.from(context).inflate(R.layout.item_list_comment, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemHolder holder = (ItemHolder) viewHolder;
        }
    }

    @Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }

    public int getBasicItemCount() {
        return 10;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     * A HeaderHolder describes an item view and metadata about its place within the RecyclerView.
     */
    class HeaderHolder extends RecyclerView.ViewHolder {
        ImageView mImgAvatar;
        TextView mTvName;
        TextView mTvDayComment;
        TextView mTvComment;

        public HeaderHolder(View view) {
            super(view);
            mImgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            mTvName = (TextView) itemView.findViewById(R.id.tvName);
            mTvDayComment = (TextView) itemView.findViewById(R.id.tvDayComment);
            mTvComment = (TextView) itemView.findViewById(R.id.tvComment);
        }
    }

    /**
     * A ItemHolder describes an item view bottom of the RecyclerView.
     */
    class ItemHolder extends RecyclerView.ViewHolder {
        public ItemHolder(View itemView) {
            super(itemView);
        }
    }
}
