package com.devdn.ctc.hunterist.utils.animationviewpager;

/**
 *  Created by cuongnv on 24/11/15.
 */
public class IncompatibleRatioException extends RuntimeException {

    public IncompatibleRatioException() {
        super("Can't perform Ken Burns effect on rects with distinct aspect ratios!");
    }
}
