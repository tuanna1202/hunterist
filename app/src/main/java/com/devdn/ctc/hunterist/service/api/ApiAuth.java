package com.devdn.ctc.hunterist.service.api;

import com.devdn.ctc.hunterist.model.common.User;
import com.devdn.ctc.hunterist.service.api.core.ApiClient;

import retrofit.Call;
import retrofit.Callback;

/**
 * Project RetrofitDemo.
 * Copyright © 2015.
 * Created by tuanna.
 */
public class ApiAuth {
    public static void getUser(String userName, Callback callback){
        Call<User> call = ApiClient.Builder().getUser(userName);
        call.enqueue(callback);
    }
}
