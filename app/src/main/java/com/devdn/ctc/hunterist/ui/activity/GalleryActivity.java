package com.devdn.ctc.hunterist.ui.activity;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.model.ImageOnPhone;
import com.devdn.ctc.hunterist.ui.adapter.GalleryAdapter;
import com.devdn.ctc.hunterist.utils.SavingFileUtil;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.util.ArrayList;

/**
 * Gallery screen that uses to get all photo from device
 *
 * @author CuongNV
 */
@EActivity(R.layout.activity_gallery)
public class GalleryActivity extends BaseActivity {

    public static final String ARG_PATH = "arg_path";
    private static final int REQUEST_TAKE_PHOTO_ACTIVITY = 100;
    public static final int COLUMN_NUMBER = 4;

    /**
     * Data set
     */
    private final ArrayList<ImageOnPhone> mImgOnPhones = new ArrayList<>();
    private GalleryAdapter mAdapter;
    private String mCurrentPhotoPath = "";

    @ViewById
    RecyclerView mRecyclerView;

    @Override
    protected void init() {
        mAdapter = new GalleryAdapter(this, mImgOnPhones, new GalleryAdapter.OnGalleryAdapterListener() {
            @Override
            public void onPhotoItemClick(ImageOnPhone imageOnPhone) {
                finishWithImagePath(imageOnPhone.getPath());
            }

            @Override
            public void onCameraItemClick() {
                Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (intentCamera.resolveActivity(getPackageManager()) != null) {
                    File photoFile = SavingFileUtil.getOutputMediaFile();
                    if (photoFile == null) {
                        return;
                    }
                    intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                    mCurrentPhotoPath = photoFile.getAbsolutePath();
                    startActivityForResult(intentCamera, REQUEST_TAKE_PHOTO_ACTIVITY);
                }
            }
        });

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new GridLayoutManager(this, COLUMN_NUMBER));
        mRecyclerView.addItemDecoration(new GridSpacingItemDecoration(COLUMN_NUMBER, getResources().getDimensionPixelSize(R.dimen.grid_gallery_spacing), false));
        mRecyclerView.setAdapter(mAdapter);
        //load data
        new LoadingImageFromGalleryOnPhone().execute();
    }

    private void finishWithImagePath(@NonNull String path) {
        Intent returnIntent = new Intent();
        returnIntent.putExtra(ARG_PATH, path);
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    @OnActivityResult(REQUEST_TAKE_PHOTO_ACTIVITY)
    void onResultTakePhoto(int resultCode) {
        if (resultCode == RESULT_OK) {
            if (!TextUtils.isEmpty(mCurrentPhotoPath)) {
                galleryAddPic();
                finishWithImagePath(mCurrentPhotoPath);
            }
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    /**
     * Class using get all photo in gallery of device
     */
    private class LoadingImageFromGalleryOnPhone extends AsyncTask<Void, Void, ArrayList<ImageOnPhone>> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected ArrayList<ImageOnPhone> doInBackground(Void... params) {
            ArrayList<ImageOnPhone> imageOnPhones = new ArrayList<ImageOnPhone>();

            // Columns
            String[] columns = {MediaStore.Images.Media.DATA,
                    MediaStore.Images.Media._ID};

            // Order By
            String sortOrder = MediaStore.Images.Media._ID;

            // Cursor
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                    columns, null, null, sortOrder);

            if (cursor == null) {
                return null;
            }

            // Count
            int count = cursor.getCount();

            // ImageOnPhone
            ImageOnPhone imageOnPhone;

            // Data Index
            int idColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID);
            int dataColumnIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            int id;
            String path;
            for (int i = count - 1; i >= 0; i--) {
                if (isCancelled()) {
                    break;
                }

                cursor.moveToPosition(i);
                id = cursor.getInt(idColumnIndex);
                path = cursor.getString(dataColumnIndex);
                imageOnPhone = new ImageOnPhone();
                imageOnPhone.setId(id);
                imageOnPhone.setPath(path);
                imageOnPhones.add(imageOnPhone);
            }
            cursor.close();
            return imageOnPhones;
        }

        @Override
        protected void onPostExecute(ArrayList<ImageOnPhone> result) {
            super.onPostExecute(result);
            if (isCancelled()) {
                result = null;
            }

            mImgOnPhones.clear();

            if (result != null && !result.isEmpty()) {
                mImgOnPhones.addAll(0, result);
            }
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Class using create space between item in RecyclerView.
     */
    private class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int mSpanCount;
        private int mSpacing;
        private boolean mIncludeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            mSpanCount = spanCount;
            mSpacing = spacing;
            mIncludeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildLayoutPosition(view); // item position
            int column = position % mSpanCount; // item column

            if (mIncludeEdge) {
                outRect.left = mSpacing - column * mSpacing / mSpanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * mSpacing / mSpanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < mSpanCount) { // top edge
                    outRect.top = mSpacing;
                }
                outRect.bottom = mSpacing; // item bottom
            } else {
                outRect.left = column * mSpacing / mSpanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = mSpacing - (column + 1) * mSpacing / mSpanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= mSpanCount) {
                    outRect.top = mSpacing; // item top
                }
            }
        }
    }
}
