package com.devdn.ctc.hunterist.model.common;

import java.util.List;

import lombok.Data;
import lombok.experimental.Builder;

/**
 * Project HunterIst.
 * Copyright © 2015.
 * Created by tuanna.
 */
@Data
@Builder
public class Note {
    int like;
    String imgUrl;
    List<Comment> commentList;

    @Data
    public class Comment {

    }
}
