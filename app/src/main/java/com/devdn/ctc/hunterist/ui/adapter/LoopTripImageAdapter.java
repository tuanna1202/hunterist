package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.model.common.Trip;
import com.squareup.picasso.Picasso;

/**
 * Project HunterIst.
 * Copyright © 2015.
 * Created by tuanna.
 */
public class LoopTripImageAdapter extends PagerAdapter {
    private Trip mTrip;
    private LayoutInflater inflater;
    private Context mContext;

    public LoopTripImageAdapter(Context context, Trip trip) {
        super();
        this.mTrip = trip;
        this.mContext = context;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = inflater.inflate(R.layout.item_loop_trip, null);
        ImageView mImgChipPhoto = (ImageView) view.findViewById(R.id.mImgChipPhoto);
        ImageView imgPrev = (ImageView) view.findViewById(R.id.imgPrev);
        ImageView imgNext = (ImageView) view.findViewById(R.id.imgNext);
        final ViewPager viewPager = (ViewPager) container;
        final int currentPosition = position;
        imgPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(currentPosition - 1);
            }
        });
        imgNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewPager.setCurrentItem(currentPosition + 1);
            }
        });
        Picasso.with(mContext).load(mTrip.getNoteList().get(position).getImgUrl()).into(mImgChipPhoto);
        container.addView(view);
        return view;
    }

    /**
     * Return the number of views available.
     */
    @Override
    public int getCount() {
        return mTrip.getNoteList().size();
    }

    /**
     * @param view   Page View to check for association with <code>object</code>
     * @param object Object to check for association with <code>view</code>
     * @return true if <code>view</code> is associated with the key object <code>object</code>
     */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
}
