package com.devdn.ctc.hunterist.ui.fragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.model.common.MyColor;
import com.devdn.ctc.hunterist.ui.activity.AccountSettingActivity_;
import com.devdn.ctc.hunterist.ui.activity.ConnectionActivity_;
import com.devdn.ctc.hunterist.ui.activity.MainActivity_;
import com.devdn.ctc.hunterist.ui.activity.NotificationActivity_;
import com.devdn.ctc.hunterist.ui.activity.TripActivity_;
import com.devdn.ctc.hunterist.ui.activity.ViewProfileActivity_;
import com.devdn.ctc.hunterist.ui.activity.welcome.LoginActivity_;
import com.devdn.ctc.hunterist.ui.activity.welcome.RegisterActivity_;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 *  Created by cuongnv on 06/11/15.
 */
@EFragment(R.layout.fragment_navigation_menu)
public class NavBarFragment extends BaseFragment {

    @ViewById
    View mAvatarContainer;
    @ViewById
    View containerLogin;
    @ViewById
    View containerHome;
    @ViewById
    View containerTrips;
    @ViewById
    View containerConnects;
    @ViewById
    View containerNotification;
    @ViewById
    View containerAbout;
    @ViewById
    View containerProfile;
    @ViewById
    View containerSetting;

    @ViewById
    ImageView mImgLogin;
    @ViewById
    ImageView mImgHome;
    @ViewById
    ImageView mImgTrips;
    @ViewById
    ImageView mImgConnections;
    @ViewById
    ImageView mImgNotification;
    @ViewById
    ImageView mImgAbout;
    @ViewById
    ImageView mImgProfile;
    @ViewById
    ImageView mImgSetting;
    @ViewById
    TextView mTvLogin;
    @ViewById
    TextView mTvHome;
    @ViewById
    TextView mTvTrips;
    @ViewById
    TextView mTvConnections;
    @ViewById
    TextView mTvNotification;
    @ViewById
    TextView mTvAbout;
    @ViewById
    TextView mTvProfile;
    @ViewById
    TextView mTvSetting;

    @Override
    protected void init() {
        // TODO
    }

    @Override
    public void onResume() {
        super.onResume();
        clearBackgroundFocus();
    }

    @Click(R.id.mAvatarContainer)
    void onAvatarClick(){
        RegisterActivity_.intent(getContext()).start();
    }
    
    @Click(R.id.containerLogin)
    void onLoginClick() {
        setBackgroundColor(containerLogin);
        LoginActivity_.intent(getContext()).start();
    }

    @Click(R.id.containerHome)
    void onAddTripClick() {
        setBackgroundColor(containerHome);
        MainActivity_.intent(getContext()).start();
    }

    @Click(R.id.containerTrips)
    void onTripsClick() {
        setBackgroundColor(containerTrips);
        TripActivity_.intent(getContext()).start();
    }

    @Click(R.id.containerConnects)
    void onConnectionsClick() {
        setBackgroundColor(containerConnects);
        ConnectionActivity_.intent(getContext()).start();
    }

    @Click(R.id.containerNotification)
    void onNotificationClick() {
        setBackgroundColor(containerNotification);
        NotificationActivity_.intent(getContext()).start();
    }

    @Click(R.id.containerAbout)
    void onAboutClick() {
        setBackgroundColor(containerAbout);
    }

    @Click(R.id.containerProfile)
    void onProfileClick() {
        setBackgroundColor(containerProfile);
        ViewProfileActivity_.intent(getContext()).start();
    }

    @Click(R.id.containerSetting)
    void onSettingClick() {
        setBackgroundColor(containerSetting);
        AccountSettingActivity_.intent(getContext()).start();
    }

    // set background for item clickEnable
    private void setBackgroundColor(View view) {
        if (view.equals(containerHome)) {
            containerHome.setBackgroundColor(MyColor.GRAY);
            mTvHome.setTextColor(MyColor.MAIN_COLOR);
            mImgHome.setImageResource(R.drawable.ic_home_press);
        } else {
            containerHome.setBackgroundColor(MyColor.WHITE);
            mTvHome.setTextColor(MyColor.BLACK);
            mImgHome.setImageResource(R.drawable.ic_home);
        }
        if (view.equals(containerTrips)) {
            containerTrips.setBackgroundColor(MyColor.GRAY);
            mTvTrips.setTextColor(MyColor.MAIN_COLOR);
            mImgTrips.setImageResource(R.drawable.ic_trips_press);
        } else {
            containerTrips.setBackgroundColor(MyColor.WHITE);
            mTvTrips.setTextColor(MyColor.BLACK);
            mImgTrips.setImageResource(R.drawable.ic_trips);
        }
        if (view.equals(containerConnects)) {
            containerConnects.setBackgroundColor(MyColor.GRAY);
            mTvConnections.setTextColor(MyColor.MAIN_COLOR);
            mImgConnections.setImageResource(R.drawable.ic_connections_press);
        } else {
            containerConnects.setBackgroundColor(MyColor.WHITE);
            mTvConnections.setTextColor(MyColor.BLACK);
            mImgConnections.setImageResource(R.drawable.ic_connections);
        }
        if (view.equals(containerNotification)) {
            containerNotification.setBackgroundColor(MyColor.GRAY);
            mTvNotification.setTextColor(MyColor.MAIN_COLOR);
            mImgNotification.setImageResource(R.drawable.ic_notification_press);
        } else {
            containerNotification.setBackgroundColor(MyColor.WHITE);
            mTvNotification.setTextColor(MyColor.BLACK);
            mImgNotification.setImageResource(R.drawable.ic_notification);
        }
        if (view.equals(containerAbout)) {
            containerAbout.setBackgroundColor(MyColor.GRAY);
            mTvAbout.setTextColor(MyColor.MAIN_COLOR);
            mImgAbout.setImageResource(R.drawable.ic_about_press);
        } else {
            containerAbout.setBackgroundColor(MyColor.WHITE);
            mTvAbout.setTextColor(MyColor.BLACK);
            mImgAbout.setImageResource(R.drawable.ic_about);
        }
        if (view.equals(containerProfile)) {
            containerProfile.setBackgroundColor(MyColor.GRAY);
            mTvProfile.setTextColor(MyColor.MAIN_COLOR);
            mImgProfile.setImageResource(R.drawable.ic_profile_press);
        } else {
            containerProfile.setBackgroundColor(MyColor.WHITE);
            mTvProfile.setTextColor(MyColor.BLACK);
            mImgProfile.setImageResource(R.drawable.ic_profile);
        }
        if (view.equals(containerSetting)) {
            containerSetting.setBackgroundColor(MyColor.GRAY);
            mTvSetting.setTextColor(MyColor.MAIN_COLOR);
            mImgSetting.setImageResource(R.drawable.ic_setting_press);
        } else {
            containerSetting.setBackgroundColor(MyColor.WHITE);
            mTvSetting.setTextColor(MyColor.BLACK);
            mImgSetting.setImageResource(R.drawable.ic_setting);
        }
        if (view.equals(containerLogin)) {
            containerLogin.setBackgroundColor(MyColor.GRAY);
            mTvLogin.setTextColor(MyColor.MAIN_COLOR);
            mImgLogin.setImageResource(R.drawable.ic_login_press);
        } else {
            containerLogin.setBackgroundColor(MyColor.WHITE);
            mTvLogin.setTextColor(MyColor.BLACK);
            mImgLogin.setImageResource(R.drawable.ic_login);
        }
    }

    // class to clear background focus. Reset background default
    private void clearBackgroundFocus() {
        containerHome.setBackgroundColor(MyColor.WHITE);
        mTvHome.setTextColor(MyColor.BLACK);
        mImgHome.setImageResource(R.drawable.ic_home);
        containerTrips.setBackgroundColor(MyColor.WHITE);
        mTvTrips.setTextColor(MyColor.BLACK);
        mImgTrips.setImageResource(R.drawable.ic_trips);
        containerConnects.setBackgroundColor(MyColor.WHITE);
        mTvConnections.setTextColor(MyColor.BLACK);
        mImgConnections.setImageResource(R.drawable.ic_connections);
        containerNotification.setBackgroundColor(MyColor.WHITE);
        mTvNotification.setTextColor(MyColor.BLACK);
        mImgNotification.setImageResource(R.drawable.ic_notification);
        containerAbout.setBackgroundColor(MyColor.WHITE);
        mTvAbout.setTextColor(MyColor.BLACK);
        mImgAbout.setImageResource(R.drawable.ic_about);
        containerProfile.setBackgroundColor(MyColor.WHITE);
        mTvProfile.setTextColor(MyColor.BLACK);
        mImgProfile.setImageResource(R.drawable.ic_profile);
        containerSetting.setBackgroundColor(MyColor.WHITE);
        mTvSetting.setTextColor(MyColor.BLACK);
        mImgSetting.setImageResource(R.drawable.ic_setting);
        containerLogin.setBackgroundColor(MyColor.WHITE);
        mTvLogin.setTextColor(MyColor.BLACK);
        mImgLogin.setImageResource(R.drawable.ic_login);
    }
}
