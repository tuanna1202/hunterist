package com.devdn.ctc.hunterist.ui.activity.welcome;

import android.graphics.PorterDuff;
import android.os.Handler;
import android.widget.ProgressBar;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.activity.BaseActivity;
import com.devdn.ctc.hunterist.ui.activity.HomeActivity_;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;


/**
 *  Created by cuongnv on 06/11/15.
 */
@EActivity(R.layout.splash_screen)
public class SplashActivity extends BaseActivity {
    @ViewById
    ProgressBar mProgressLoading;
    private int progressStatus;
    private Handler handler = new Handler();

    @Override
    protected void init() {
        mProgressLoading.getProgressDrawable().setColorFilter(0xff04AFEF, PorterDuff.Mode.SRC_IN);
        // Start long running operation in a background thread
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 50) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            mProgressLoading.setProgress(progressStatus);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (progressStatus == 50) {
                        HomeActivity_.intent(SplashActivity.this).start();
                        finish();
                    }
                }
            }
        }).start();
    }
}
