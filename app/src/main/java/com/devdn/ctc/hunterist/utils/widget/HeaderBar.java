package com.devdn.ctc.hunterist.utils.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.devdn.ctc.hunterist.R;

/**
 * Custom HeaderBar.
 *
 * @author CuongNV
 */
public class HeaderBar extends Toolbar implements View.OnClickListener {
    /**
     * Interface definition for a callback to be invoked when a view is clicked.
     */
    public interface OnHeaderBarListener {
        void onLeftButtonClick(View v);

        void onRightButtonClick(View v);
    }

    private OnHeaderBarListener mListener;

    private int mHeaderHeight;

    private TextView mTvTitle;
    private ImageView mImgLeft;
    private ImageView mImgRight;
    private View mBottomLine;
    private RelativeLayout mHeaderContainer;

    public HeaderBar(Context context) {
        this(context, null);
    }

    public HeaderBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HeaderBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        View view = LayoutInflater.from(context).inflate(R.layout.header_bar, this, false);
        addView(view);

        mHeaderHeight = getResources().getDimensionPixelSize(R.dimen.header_bar_height);
        mTvTitle = (TextView) view.findViewById(R.id.mTvTitle);
        mImgLeft = (ImageView) view.findViewById(R.id.mImgLeft);
        mImgRight = (ImageView) view.findViewById(R.id.mImgRight);
        mBottomLine = view.findViewById(R.id.mBottomLine);
        mHeaderContainer = (RelativeLayout) view.findViewById(R.id.mHeaderContainer);

        mTvTitle.setOnClickListener(this);
        mImgLeft.setOnClickListener(this);
        mImgRight.setOnClickListener(this);

        setDefaultUI();
        setAttributeSet(context, attrs);
    }

    private void setDefaultUI() {
        mImgLeft.setVisibility(View.GONE);
        mImgRight.setVisibility(View.INVISIBLE);
        mBottomLine.setVisibility(View.GONE);
    }

    private void setAttributeSet(Context context, AttributeSet attrs) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.HeaderBar);

        // get
        String title = a.getString(R.styleable.HeaderBar_mHeaderTitle);
        int leftIcon = a.getResourceId(R.styleable.HeaderBar_mLeftIcon, 0);
        int rightIcon = a.getResourceId(R.styleable.HeaderBar_mRightIcon, 0);
        int bottomLineVisibility = a.getInt(R.styleable.HeaderBar_mLineBottomVisibility, 1) == 0 ? VISIBLE : GONE; // default is 1 (GONE)
        mTvTitle.setTextColor(a.getColor(R.styleable.HeaderBar_mTitleColor, ContextCompat.getColor(getContext(), R.color.main_color)));
        mHeaderContainer.setBackgroundColor(a.getColor(R.styleable.HeaderBar_mBackgroundColor, ContextCompat.getColor(getContext(), android.R.color.white)));
        a.recycle();

        // set
        if (title != null) {
            mTvTitle.setText(title);
        }

        if (leftIcon == 0) {
            mImgLeft.setVisibility(GONE);
        } else {
            mImgLeft.setImageResource(leftIcon);
            mImgLeft.setVisibility(VISIBLE);
        }

        if (rightIcon == 0) {
            mImgRight.setVisibility(INVISIBLE);
        } else {
            mImgRight.setImageResource(rightIcon);
            mImgRight.setVisibility(VISIBLE);
        }

        mBottomLine.setVisibility(bottomLineVisibility);
    }

    public void setOnHeaderBarListener(OnHeaderBarListener l) {
        mListener = l;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mImgLeft:
                if (mListener != null) {
                    mListener.onLeftButtonClick(v);
                } else {
                    ((Activity) getContext()).onBackPressed();
                }
                break;

            case R.id.mImgRight:
                if (mListener != null) {
                    mListener.onRightButtonClick(v);
                }
                break;
        }
    }

    public void setTitle(String title) {
        mTvTitle.setText(title);
    }

    public void setDisplayHeader(final boolean showHeader) {
        boolean visible = getVisibility() == VISIBLE;
        if (visible == showHeader) {
            return;
        }
        TranslateAnimation animation = showHeader
                ? new TranslateAnimation(0, 0, -mHeaderHeight, 0)
                : new TranslateAnimation(0, 0, 0, -mHeaderHeight);
        animation.setDuration(300);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setVisibility(showHeader ? VISIBLE : GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        startAnimation(animation);
    }

    /**
     * Class definition for a callback to be invoked when a view is clicked.
     */
    public abstract static class MyOnHeaderBarListener implements OnHeaderBarListener {
        @Override
        public void onLeftButtonClick(View v) {
        }

        @Override
        public void onRightButtonClick(View v) {
        }
    }

    public void setVisibilityForRightButton(int visibility) {
        mImgRight.setVisibility(visibility);
    }
}
