package com.devdn.ctc.hunterist.ui.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.dialog.GalleryDialog;
import com.devdn.ctc.hunterist.utils.SavingFileUtil;
import com.devdn.ctc.hunterist.utils.widget.HeaderBar;
import com.squareup.picasso.Picasso;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OnActivityResult;
import org.androidannotations.annotations.ViewById;

import java.io.File;

/**
 * Created by cuongnv on 09/11/15.
 */
@EActivity(R.layout.activity_account_setting)
public class AccountSettingActivity extends BaseActivity implements HeaderBar.OnHeaderBarListener {
    /**
     * Define enum of type photo.
     */
    private enum PhotoType {
        CAMERA, GALLERY
    }

    private static final int REQUEST_GALLERY_ACTIVITY_FOR_AVATAR = 101;
    private static final int REQUEST_GALLERY_ACTIVITY_FOR_COVER = 102;
    private static final int REQUEST_TAKE_PHOTO_ACTIVITY_FOR_AVATAR = 103;
    private static final int REQUEST_TAKE_PHOTO_ACTIVITY_FOR_COVER = 104;
    private static boolean isCheck;
    /**
     * Main Adapter
     */
    private GalleryDialog mGalleryDialog;
    private PhotoType mCurrentPhotoType;
    private String mAvatarPath;
    private String mCoverPath;

    @ViewById
    TextInputLayout inputLayoutUserName;
    @ViewById
    TextInputLayout inputLayoutDisplayName;
    @ViewById
    TextInputLayout inputLayoutLocation;
    @ViewById
    TextInputLayout inputLayoutBirthday;
    @ViewById
    TextInputLayout inputLayoutGender;
    @ViewById
    TextInputLayout inputLayoutInteresting;
    @ViewById
    TextInputLayout inputLayoutYourSelf;
    @ViewById
    EditText mEdtUserName;
    @ViewById
    EditText mEdtDisplayName;
    @ViewById
    EditText mEdtLocation;
    @ViewById
    EditText mEdtBirthday;
    @ViewById
    EditText mEdtGender;
    @ViewById
    EditText mEdtInteresting;
    @ViewById
    EditText mEdtYourSelf;
    @ViewById
    ImageView mImgUsername;
    @ViewById
    ImageView mImgDisplayName;
    @ViewById
    ImageView mImgLocation;
    @ViewById
    ImageView mImgBirthday;
    @ViewById
    ImageView mImgGender;
    @ViewById
    ImageView mImgInteresting;
    @ViewById
    ImageView mImgYourSelf;
    @ViewById
    ImageView mImgCamera;
    @ViewById
    HeaderBar mHeaderBar;
    @ViewById
    ImageView mImgAvatar;

    @Override
    protected void init() {
        setEvent();
    }

    private void setEvent() {
        mEdtUserName.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtDisplayName.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtLocation.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtBirthday.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtGender.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtInteresting.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtYourSelf.setOnFocusChangeListener(new MyTextFocusChange());
        /*Set text change*/
        mEdtUserName.addTextChangedListener(new MyTextWatcher(mEdtUserName));
        mEdtDisplayName.addTextChangedListener(new MyTextWatcher(mEdtDisplayName));
        mEdtLocation.addTextChangedListener(new MyTextWatcher(mEdtLocation));
        mEdtBirthday.addTextChangedListener(new MyTextWatcher(mEdtBirthday));
        mEdtGender.addTextChangedListener(new MyTextWatcher(mEdtGender));
        mEdtInteresting.addTextChangedListener(new MyTextWatcher(mEdtInteresting));
        mEdtYourSelf.addTextChangedListener(new MyTextWatcher(mEdtYourSelf));
        /*Set event for button on header bar*/
        mHeaderBar.setOnHeaderBarListener(this);
        mGalleryDialog = new GalleryDialog(this, R.style.TransparentDialog, new GalleryDialog.OnButtonClickListener() {
            @Override
            public void onCameraButtonClick() {
                if (mCurrentPhotoType == PhotoType.CAMERA) {
                    openCamera(REQUEST_TAKE_PHOTO_ACTIVITY_FOR_AVATAR);
                } else {
                    openCamera(REQUEST_TAKE_PHOTO_ACTIVITY_FOR_COVER);
                }
            }

            @Override
            public void onGalleryButtonClick() {
                if (mCurrentPhotoType == PhotoType.CAMERA) {
                    GalleryActivity_.intent(AccountSettingActivity.this).startForResult(REQUEST_GALLERY_ACTIVITY_FOR_AVATAR);
                } else {
                    GalleryActivity_.intent(AccountSettingActivity.this).startForResult(REQUEST_GALLERY_ACTIVITY_FOR_COVER);
                }
            }
        });
    }

    @Click(R.id.mImgCamera)
    void onCameraButtonClick() {
        isCheck = !isCheck;
        mImgCamera.setSelected(isCheck);
        mCurrentPhotoType = PhotoType.CAMERA;
        mGalleryDialog.show();
    }

    @Override
    public void onLeftButtonClick(View v) {
        onBackPressed();
    }

    @Override
    public void onRightButtonClick(View v) {
        submitForm();
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateUserName()) {
            return;
        }
        if (!validateDisplayName()) {
            return;
        }
        if (!validateLocation()) {
            return;
        }
        if (!validateBirthday()) {
            return;
        }
        if (!validateGender()) {
            return;
        }
        if (!validateInteresting()) {
            return;
        }
        if (!validateYourself()) {
            return;
        }
        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateUserName() {
        if (mEdtUserName.getText().toString().trim().isEmpty()) {
            inputLayoutUserName.setError(getString(R.string.err_msg_username));
            requestFocus(mEdtUserName);
            return false;
        } else {
            inputLayoutUserName.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateDisplayName() {
        if (mEdtDisplayName.getText().toString().trim().isEmpty()) {
            inputLayoutDisplayName.setError(getString(R.string.err_msg_display_name));
            requestFocus(mEdtDisplayName);
            return false;
        } else {
            inputLayoutDisplayName.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateLocation() {
        if (mEdtLocation.getText().toString().trim().isEmpty()) {
            inputLayoutLocation.setError(getString(R.string.err_msg_location));
            requestFocus(mEdtLocation);
            return false;
        } else {
            inputLayoutLocation.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateBirthday() {
        if (mEdtBirthday.getText().toString().trim().isEmpty()) {
            inputLayoutBirthday.setError(getString(R.string.err_msg_birthday));
            requestFocus(mEdtBirthday);
            return false;
        } else {
            inputLayoutBirthday.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateGender() {
        if (mEdtGender.getText().toString().trim().isEmpty()) {
            inputLayoutGender.setError(getString(R.string.err_msg_gender));
            requestFocus(mEdtGender);
            return false;
        } else {
            inputLayoutGender.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateInteresting() {
        if (mEdtInteresting.getText().toString().trim().isEmpty()) {
            inputLayoutInteresting.setError(getString(R.string.err_msg_interesting));
            requestFocus(mEdtInteresting);
            return false;
        } else {
            inputLayoutInteresting.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateYourself() {
        if (mEdtYourSelf.getText().toString().trim().isEmpty()) {
            inputLayoutYourSelf.setError(getString(R.string.err_msg_yourself));
            requestFocus(mEdtYourSelf);
            return false;
        } else {
            inputLayoutYourSelf.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.mEdtUserName:
                    validateUserName();
                    break;
                case R.id.mEdtDisplayName:
                    validateDisplayName();
                    break;
                case R.id.mEdtLocation:
                    validateLocation();
                    break;
                case R.id.mEdtBirthday:
                    validateBirthday();
                    break;
                case R.id.mEdtGender:
                    validateGender();
                    break;
                case R.id.mEdtInteresting:
                    validateInteresting();
                    break;
                case R.id.mEdtYourSelf:
                    validateYourself();
                    break;
            }
        }
    }

    private class MyTextFocusChange implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean isFocus) {
            switch (view.getId()) {
                case R.id.mEdtUserName:
                    mImgUsername.setImageResource(isFocus ? R.drawable.ic_pass_press : R.drawable.ic_pass);
                    break;
                case R.id.mEdtDisplayName:
                    mImgDisplayName.setImageResource(isFocus ? R.drawable.ic_display_name_press : R.drawable.ic_display_name);
                    break;
                case R.id.mEdtLocation:
                    mImgLocation.setImageResource(isFocus ? R.drawable.ic_address_press : R.drawable.ic_address);
                    break;
                case R.id.mEdtBirthday:
                    mImgBirthday.setImageResource(isFocus ? R.drawable.ic_birth_press : R.drawable.ic_birth);
                    break;
                case R.id.mEdtGender:
                    mImgGender.setImageResource(isFocus ? R.drawable.ic_gender_press : R.drawable.ic_gender);
                    break;
                case R.id.mEdtInteresting:
                    mImgInteresting.setImageResource(isFocus ? R.drawable.ic_favorites_press : R.drawable.ic_favorites);
                    break;
                case R.id.mEdtYourSelf:
                    mImgYourSelf.setImageResource(isFocus ? R.drawable.ic_desciption_press : R.drawable.ic_desciption);
                    break;
            }
        }
    }

    private void openCamera(int requestCode) {
        Intent intentCamera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (intentCamera.resolveActivity(getPackageManager()) != null) {
            File photoFile = SavingFileUtil.getOutputMediaFile();
            if (photoFile == null) {
                return;
            }
            intentCamera.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
            if (mCurrentPhotoType == PhotoType.CAMERA) {
                mAvatarPath = photoFile.getAbsolutePath();
            } else {
                mCoverPath = photoFile.getAbsolutePath();
            }
            startActivityForResult(intentCamera, requestCode);
        }
    }

    @OnActivityResult(REQUEST_GALLERY_ACTIVITY_FOR_AVATAR)
    void onResultForAvatar(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK || data == null) {
            return;
        }
        mAvatarPath = data.getStringExtra(GalleryActivity.ARG_PATH);
        Picasso.with(this)
                .load("file://" + mAvatarPath)
                .centerCrop()
                .fit()
                .into(mImgAvatar);
    }

    @OnActivityResult(REQUEST_GALLERY_ACTIVITY_FOR_COVER)
    void onResultGalleryForCover(int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK || data == null) {
            return;
        }
        mCoverPath = data.getStringExtra(GalleryActivity.ARG_PATH);
        Picasso.with(this)
                .load("file://" + mCoverPath)
                .centerCrop()
                .fit()
                .into(mImgAvatar);
    }

    @OnActivityResult(REQUEST_TAKE_PHOTO_ACTIVITY_FOR_AVATAR)
    void onResultGalleryCameraForAvatar(int resultCode) {
        if (resultCode != Activity.RESULT_OK || TextUtils.isEmpty(mAvatarPath)) {
            return;
        }
        Picasso.with(this)
                .load("file://" + mAvatarPath)
                .centerCrop()
                .fit()
                .into(mImgAvatar);
    }

    @OnActivityResult(REQUEST_TAKE_PHOTO_ACTIVITY_FOR_COVER)
    void onResultCameraForCover(int resultCode) {
        if (resultCode != Activity.RESULT_OK || TextUtils.isEmpty(mCoverPath)) {
            return;
        }
        Picasso.with(this)
                .load("file://" + mCoverPath)
                .centerCrop()
                .fit()
                .into(mImgAvatar);
    }
}
