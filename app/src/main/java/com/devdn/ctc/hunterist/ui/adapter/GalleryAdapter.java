package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.model.ImageOnPhone;
import com.devdn.ctc.hunterist.ui.activity.GalleryActivity;
import com.devdn.ctc.hunterist.utils.ScreenUtil;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Gallery Adapter.
 *
 * @author CuongNV
 */
public class GalleryAdapter extends BaseAdapter {
    /**
     * Define interface for this Adapter
     */
    public interface OnGalleryAdapterListener {
        void onPhotoItemClick(ImageOnPhone imageOnPhone);

        void onCameraItemClick();
    }

    /**
     * Define Type for this Adapter
     */
    private enum Type {
        CAMERA_ITEM(0),
        PHOTO_ITEM(1);

        private final int value;

        Type(int val) {
            value = val;
        }

        public int getValue() {
            return value;
        }
    }

    private final ArrayList<ImageOnPhone> mImgOnPhones;
    private final OnGalleryAdapterListener mListener;
    private final int mPhotoSize;

    public GalleryAdapter(@NonNull Context context, @NonNull ArrayList<ImageOnPhone> list, @NonNull OnGalleryAdapterListener l) {
        super(context);
        this.mImgOnPhones = list;
        this.mListener = l;
        int screenWidth = ScreenUtil.getWidthScreen(context) * 462 / 750;
        int gridMargin = getResources().getDimensionPixelSize(R.dimen.grid_gallery_spacing);
        mPhotoSize = (screenWidth - gridMargin * (GalleryActivity.COLUMN_NUMBER - 1)) / GalleryActivity.COLUMN_NUMBER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == Type.CAMERA_ITEM.getValue()) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_gallery_camera, parent, false);
            view.getLayoutParams().height = mPhotoSize;
            return new CameraHolder(view);
        }
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_gallery_photo, parent, false);
        view.getLayoutParams().height = mPhotoSize;
        return new PhotoHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == Type.PHOTO_ITEM.getValue()) {
            onBindPhotoHolder((PhotoHolder) holder, mImgOnPhones.get(position - 1));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return Type.CAMERA_ITEM.getValue();
        } else {
            return Type.PHOTO_ITEM.getValue();
        }
    }

    @Override
    public int getItemCount() {
        return mImgOnPhones.size() + 1;
    }

    private void onBindPhotoHolder(PhotoHolder holder, ImageOnPhone imageOnPhone) {
        if (!TextUtils.isEmpty(imageOnPhone.getPath())) {
            Picasso.with(getContext())
                    .load(new File(imageOnPhone.getPath()))
                    .centerCrop()
                    .fit()
                    .into(holder.mImgPhoto);
        } else {
            Picasso.with(getContext())
                    .load(R.drawable.bg_image_transparent)
                    .centerCrop()
                    .fit()
                    .into(holder.mImgPhoto);
        }
    }

    /**
     * ViewHolder for Camera Item.
     */
    private class CameraHolder extends RecyclerView.ViewHolder {
        private ImageView mImgCamera;

        public CameraHolder(View itemView) {
            super(itemView);
            mImgCamera = (ImageView) itemView.findViewById(R.id.mImgCamera);
            mImgCamera.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onCameraItemClick();
                }
            });
        }
    }

    /**
     * ViewHolder for Photo Item.
     */
    private class PhotoHolder extends RecyclerView.ViewHolder {
        private ImageView mImgPhoto;

        public PhotoHolder(View itemView) {
            super(itemView);
            mImgPhoto = (ImageView) itemView.findViewById(R.id.mImgPhoto);
            mImgPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onPhotoItemClick(mImgOnPhones.get(getLayoutPosition() - 1));
                }
            });
        }
    }
}
