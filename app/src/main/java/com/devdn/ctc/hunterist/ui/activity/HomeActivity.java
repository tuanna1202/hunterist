package com.devdn.ctc.hunterist.ui.activity;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.adapter.HomeAdapter;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by CuongNV on 06/11/15.
 */
@EActivity(R.layout.activity_home)
public class HomeActivity extends BaseActivity {
    private static final int PERIOD = 2000;
    /**
     * Main Adapter
     */
    private HomeAdapter mAdapter;
    /**
     * true if header bar is running animation
     */
    private boolean isRunningAnim;
    private long lastPressedTime;

    @ViewById
    View mLayoutHeader;
    @ViewById
    RecyclerView mRecyclerView;
    @ViewById
    DrawerLayout mMyPageDrawerLayout;

    @Override
    protected void init() {
        mAdapter = new HomeAdapter(this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 20) {
                    // hide header bar
                    displayHeaderBar(false);
                } else if (dy < 0) {
                    // ic_next header bar
                    displayHeaderBar(true);
                }
            }
        });
    }

    private void displayHeaderBar(final boolean showHeader) {
        if (isRunningAnim) {
            return;
        }

        boolean visible = mLayoutHeader.getVisibility() == View.VISIBLE;
        if (visible == showHeader) {
            return;
        }

        int headerHeight = getResources().getDimensionPixelSize(R.dimen.header_bar_height);
        TranslateAnimation animation = showHeader
                ? new TranslateAnimation(0, 0, -headerHeight, 0)
                : new TranslateAnimation(0, 0, 0, -headerHeight);
        animation.setDuration(getResources().getInteger(R.integer.animation_duration));
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                isRunningAnim = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isRunningAnim = false;
                mLayoutHeader.setVisibility(showHeader ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        mLayoutHeader.startAnimation(animation);
    }

    @Click(R.id.mImgMenu)
    void onMyPageClick() {
        mMyPageDrawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_DOWN:
                    if (mMyPageDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                        mMyPageDrawerLayout.closeDrawers();
                        return true;
                    } else {
                        if (event.getDownTime() - lastPressedTime < PERIOD) {
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), "Press again to exit.",
                                    Toast.LENGTH_SHORT).show();
                            lastPressedTime = event.getEventTime();
                        }
                    }
                    return true;
            }
        }
        return false;
    }
}
