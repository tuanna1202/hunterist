package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;

/**
 * Created by cuongnv on 08/11/15.
 */
public class ConnectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /**
     * This enum defines type view.
     */
    private enum Type {
        TYPE_ITEM(1), TYPE_HEADER(2);

        private final int value;

        Type(int val) {
            this.value = val;
        }

        public int getValue() {
            return value;
        }
    }

    private Context mContext;

    public ConnectionAdapter(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == Type.TYPE_ITEM.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.custom_item_connections, parent, false);
            return new ItemHolder(view);
        } else if (viewType == Type.TYPE_HEADER.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.header_bar_recyclerview, parent, false);
            return new HeaderHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemHolder holder = (ItemHolder) viewHolder;
        }
    }

    @Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return Type.TYPE_HEADER.getValue();
        }
        return Type.TYPE_ITEM.getValue();
    }

    public int getBasicItemCount() {
        return 10;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     * A ItemHolder describes an item view and metadata about its place within the RecyclerView.
     */
    class HeaderHolder extends RecyclerView.ViewHolder {
        public HeaderHolder(View view) {
            super(view);
        }
    }

    /**
     * A ItemHolder describes an item view bottom of the RecyclerView.
     */
    class ItemHolder extends RecyclerView.ViewHolder {
        RelativeLayout mRlItemConnect;
        ImageView mImgFriend;
        TextView mTvNameFriend;
        TextView mTvLocationFriend;

        public ItemHolder(View itemView) {
            super(itemView);
            mRlItemConnect = (RelativeLayout) itemView.findViewById(R.id.rlItemConnect);
            mImgFriend = (ImageView) itemView.findViewById(R.id.imgAvatar);
            mTvNameFriend = (TextView) itemView.findViewById(R.id.tvNameFriend);
            mTvLocationFriend = (TextView) itemView.findViewById(R.id.tvLocationFriend);
            mRlItemConnect.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(mContext, "Click connect friend ", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
