package com.devdn.ctc.hunterist.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.adapter.CommentAdapter;

import org.androidannotations.annotations.AfterTextChange;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_comment)
public class CommentActivity extends BaseActivity {
    /**
     * Comment Adapter using screen comments
     */
    private CommentAdapter mAdapter;

    @ViewById
    RecyclerView recyclerViewComment;
    @ViewById
    ImageView mImgComment;
    @ViewById
    View headerComment;
    @ViewById
    EditText mEdtComment;

    @Override
    protected void init() {
        mAdapter = new CommentAdapter(this);
        recyclerViewComment.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewComment.setAdapter(mAdapter);
    }

    @Click(R.id.mImgComment)
    void onCommentButtonClick() {
        Toast.makeText(this, "Click comment", Toast.LENGTH_SHORT).show();
    }

    @TextChange(R.id.mEdtComment)
    void onTextChangedComment(CharSequence text) {
        mImgComment.setImageResource(text.length()>0? R.drawable.ic_send_message_press: R.drawable.ic_send_message);
        mImgComment.setClickable(text.length()>0? true: false);
    }

    @Click(R.id.headerComment)
    void onHeaderCommentClick() {
        NumberLikeActivity_.intent(this).start();
    }
}
