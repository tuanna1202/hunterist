package com.devdn.ctc.hunterist.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.adapter.ConnectionAdapter;
import com.devdn.ctc.hunterist.ui.adapter.MainAdapter;
import com.devdn.ctc.hunterist.utils.widget.HeaderBar;
import com.devdn.ctc.hunterist.utils.widget.HidingScrollListener;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by cuongnv on 08/11/15.
 */
@EActivity(R.layout.activity_connection)
public class ConnectionActivity extends BaseActivity {
    /**
     * Main Adapter
     */
    private ConnectionAdapter mAdapter;
    @ViewById(R.id.recyclerViewConnect)
    protected RecyclerView mRecyclerViewConnect;

    @ViewById
    HeaderBar mHeaderBar;

    @Override
    protected void init() {
        mAdapter = new ConnectionAdapter(this);
        mRecyclerViewConnect.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewConnect.setHasFixedSize(true);
        mRecyclerViewConnect.setAdapter(mAdapter);
        mRecyclerViewConnect.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    private void hideViews() {
        mHeaderBar.animate().translationY(-mHeaderBar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        mHeaderBar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }
}
