package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.activity.LoopTripActivity_;
import com.squareup.picasso.Picasso;

/**
 * Created by CuongNV on 06/11/15.
 */
public class HomeAdapter extends BaseAdapter<HomeAdapter.TopHolder> {
    private Context mContext;

    public HomeAdapter(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }

    @Override
    public TopHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.item_list_home, parent, false);
        return new TopHolder(view);
    }

    @Override
    public void onBindViewHolder(TopHolder holder, int position) {
        String s1 ="https://d28vlf87tojvef.cloudfront.net/150/201401406611537127/bb3/34e/9e64386460a5e13df869397b01e1d937.jpg";
        String s2 ="https://d28vlf87tojvef.cloudfront.net/910/201401406611524581/bb3/34e/4689c91f41ee41123cb2b80950bd0cad.jpg";
       if (position % 2 == 0)
           Picasso.with(mContext).load(s1).into(holder.mImgPhoto);
        else
           Picasso.with(mContext).load(s2).into(holder.mImgPhoto);
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    /**
     * Define Top's ViewHolder
     */
    class TopHolder extends RecyclerView.ViewHolder{
        private final ImageView mImgPhoto;
        private final TextView mTvChipHome;

        public TopHolder(View itemView) {
            super(itemView);
            mImgPhoto = (ImageView) itemView.findViewById(R.id.mImgPhoto);
            mTvChipHome = (TextView) itemView.findViewById(R.id.mTvChipHome);

            mImgPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LoopTripActivity_.intent(mContext).start();
                }
            });
        }
    }
}
