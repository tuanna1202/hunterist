package com.devdn.ctc.hunterist.utils;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

//import retrofit.mime.TypedFile;

public final class SavingFileUtil {
    public static final String TAG = SavingFileUtil.class.getSimpleName();
    public static final String APP_NAME = "Vivo";

    private SavingFileUtil() {
        // no instance
    }

    /**
     * Create a file Uri for saving an image
     */
    public static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    /**
     * Create a File for saving an image
     */
    public static File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), APP_NAME);

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.getDefault()).format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + APP_NAME + ".jpg");
    }

    public static void writeBitmapToFile(Bitmap bitmap, File file) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bos);
        byte[] bitmapData = bos.toByteArray();

        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(bitmapData);
        fos.flush();
        fos.close();
    }

//    public static TypedFile convertToFile(@NonNull String path, @NonNull Bitmap bitmap) {
//        FileOutputStream out = null;
//        File imageFileName = new File(path);
//        try {
//            out = new FileOutputStream(imageFileName);
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
//            out.flush();
//        } catch (IOException e) {
//            Log.e(TAG, "Failed to convert image to JPEG", e);
//        } finally {
//            try {
//                if (out != null) {
//                    out.close();
//                }
//            } catch (IOException e) {
//                Log.e(TAG, "Failed to close output stream", e);
//            }
//        }
//        return new TypedFile("image/jpeg", imageFileName);
//    }

    public static void deleteFile(String path) {
        File fileDelete = new File(path);
        if (fileDelete.exists()) {
            if (path.contains(APP_NAME)) {
                if (fileDelete.delete()) {
                    Log.d(TAG, "file Deleted :" + path);
                } else {
                    Log.d(TAG, "file not Deleted :" + path);
                }
            }
        }
    }
}
