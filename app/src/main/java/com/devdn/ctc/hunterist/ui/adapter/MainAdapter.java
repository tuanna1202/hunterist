package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.activity.MapTripsActivity_;
import com.devdn.ctc.hunterist.ui.activity.TripsDetailActivity_;
import com.devdn.ctc.hunterist.ui.activity.ViewProfileActivity_;
import com.squareup.picasso.Picasso;
import com.devdn.ctc.hunterist.ui.activity.CommentActivity_;

/**
 *  Created by cuongnv on 11/11/15.
 */
public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * This enum defines type view.
     */
    private enum Type {
        TYPE_HEADER(0), TYPE_ITEM(1);

        private final int value;

        Type(int val) {
            this.value = val;
        }

        public int getValue() {
            return value;
        }
    }
    private Context mContext;

    public MainAdapter(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == Type.TYPE_ITEM.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.item_list_home_chip, parent, false);
            return new ItemHolder(view);
        } else if (viewType == Type.TYPE_HEADER.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.header_bar_recyclerview, parent, false);
            return new HeaderHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemHolder holder = (ItemHolder) viewHolder;

            String s1 = "https://d28vlf87tojvef.cloudfront.net/150/201401406611537127/bb3/34e/9e64386460a5e13df869397b01e1d937.jpg";
            String s2 = "https://d28vlf87tojvef.cloudfront.net/910/201401406611524581/bb3/34e/4689c91f41ee41123cb2b80950bd0cad.jpg";
            if (position % 2 == 0)
                Picasso.with(mContext).load(s1).into(holder.mImgChipPhoto);
            else
                Picasso.with(mContext).load(s2).into(holder.mImgChipPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return Type.TYPE_HEADER.getValue();
        }
        return Type.TYPE_ITEM.getValue();
    }

    public int getBasicItemCount() {
        return 10;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     * A HeaderHolder describes an item view and metadata about its place within the RecyclerView.
     */
    class HeaderHolder extends RecyclerView.ViewHolder {
        public HeaderHolder(View view) {
            super(view);
        }
    }

    /**
     * A ItemHolder describes an item view bottom of the RecyclerView.
     */
    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private LinearLayout mContainerTrip;
        private ImageView mImgGotoMap;
        private ImageView mImgPosterAvatar;
        private ImageView mImgCommentAvatar;
        private ImageView mImgChipPhoto;
        private TextView mTvNameComment;
        private TextView mTvDateComment;
        private TextView mTvLastComment;
        private TextView mTvLikesTrip;
        private TextView mTvCommentsTrip;
        private View containerSumLikes;

        public ItemHolder(View itemView) {
            super(itemView);
            mContainerTrip = (LinearLayout) itemView.findViewById(R.id.mContainerTrip);
            mImgGotoMap = (ImageView) itemView.findViewById(R.id.mImgGotoMap);
            mImgPosterAvatar = (ImageView) itemView.findViewById(R.id.mImgPosterAvatar);
            mImgCommentAvatar = (ImageView) itemView.findViewById(R.id.mImgCommentAvatar);
            mImgChipPhoto = (ImageView) itemView.findViewById(R.id.mImgChipPhoto);
            mTvNameComment = (TextView) itemView.findViewById(R.id.mTvNameComment);
            mTvDateComment = (TextView) itemView.findViewById(R.id.mTvDateComment);
            mTvLastComment = (TextView) itemView.findViewById(R.id.mTvLastComment);
            mTvLikesTrip = (TextView) itemView.findViewById(R.id.mTvLikesTrip);
            mTvCommentsTrip = (TextView) itemView.findViewById(R.id.mTvCommentsTrip);
            containerSumLikes = (View) itemView.findViewById(R.id.containerSumLikes);

            mContainerTrip.setOnClickListener(this);
            mImgGotoMap.setOnClickListener(this);
            mImgPosterAvatar.setOnClickListener(this);
            mTvLikesTrip.setOnClickListener(this);
            mTvCommentsTrip.setOnClickListener(this);
            containerSumLikes.setOnClickListener(this);
            mImgCommentAvatar.setOnClickListener(this);
            mTvNameComment.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mContainerTrip:
                    TripsDetailActivity_.intent(mContext).start();
                    break;
                case R.id.mImgGotoMap:
                    MapTripsActivity_.intent(mContext).start();
                    break;
                case R.id.mImgPosterAvatar:
                    ViewProfileActivity_.intent(mContext).start();
                    break;
                case R.id.mImgCommentAvatar:
                    ViewProfileActivity_.intent(mContext).start();
                    break;
                case R.id.mTvNameComment:
                    ViewProfileActivity_.intent(mContext).start();
                    break;
                case R.id.mTvLikesTrip:
                    Toast.makeText(mContext, "Go to likes chip", Toast.LENGTH_LONG).show();
                    break;
                case R.id.mTvCommentsTrip:
                    CommentActivity_.intent(mContext).start();
                    break;
                case R.id.containerSumLikes:
                    CommentActivity_.intent(mContext).start();
                    break;
                default:
                    break;
            }
        }
    }
}

