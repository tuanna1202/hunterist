package com.devdn.ctc.hunterist.ui.activity;

import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.WindowManager;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.model.common.Note;
import com.devdn.ctc.hunterist.model.common.Trip;
import com.devdn.ctc.hunterist.ui.adapter.LoopTripImageAdapter;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

/**
 * Project HunterIst.
 * Copyright © 2015.
 * Created by tuanna.
 */
@EActivity(R.layout.activity_loop_trips)
public class LoopTripActivity extends BaseActivity {
    private static String s1 = "https://d28vlf87tojvef.cloudfront.net/150/201401406611537127/bb3/34e/9e64386460a5e13df869397b01e1d937.jpg";
    private static String s2 = "https://d28vlf87tojvef.cloudfront.net/910/201401406611524581/bb3/34e/4689c91f41ee41123cb2b80950bd0cad.jpg";
    private static String s3 = "https://d28vlf87tojvef.cloudfront.net/915/201401400596966376/bb3/34e/a4679dcc496ae28fea2db8b65bb8bfa8.JPG";
    @ViewById
    ViewPager vpLoopTrip;
    private LoopTripImageAdapter mAdapter;
    private ArrayList<Note> mNotes;

    @Override
    protected void init() {
        this.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        this.getWindow().setGravity(Gravity.BOTTOM);
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(this.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        this.getWindow().setAttributes(layoutParams);

        setValue();
    }

    private void setValue() {
        mNotes = new ArrayList<>();
        mNotes.add(Note.builder().imgUrl(s1).build());
        mNotes.add(Note.builder().imgUrl(s2).build());
        mNotes.add(Note.builder().imgUrl(s3).build());
        Trip trip = Trip.builder().noteList(mNotes).build();
        mAdapter = new LoopTripImageAdapter(this, trip);
        vpLoopTrip.setAdapter(mAdapter);
    }
}
