package com.devdn.ctc.hunterist.utils.animationviewpager;

import android.graphics.RectF;

/**
 * Copyright © 2015 AsianTech inc.
 * Created by VinhHlb on 11/24/15.
 */
public interface TransitionGenerator {
    Transition generateNextTransition(RectF drawableBounds, RectF viewport);
}
