package com.devdn.ctc.hunterist.ui.activity;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.adapter.CommentAdapter;
import com.devdn.ctc.hunterist.ui.adapter.NumberLikeAdapter;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by chien on 23/11/2015.
 */
@EActivity(R.layout.activity_number_like)
public class NumberLikeActivity extends BaseActivity {
    /**
     * Comment Adapter using screen comments
     */
    private NumberLikeAdapter mAdapter;

    @ViewById
    RecyclerView recyclerViewNumberLike;
    @ViewById
    View containerHeaderLike;
    @ViewById
    View containerItemLike;

    @Override
    protected void init() {
        mAdapter = new NumberLikeAdapter(this);
        recyclerViewNumberLike.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewNumberLike.setAdapter(mAdapter);
    }

    @Click(R.id.containerHeaderLike)
    void onButtonHeaderLike() {
        onBackPressed();
    }

    @Click(R.id.containerItemLike)
    void onButtonItemFriend() {
    }
}
