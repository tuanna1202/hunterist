package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.activity.ConnectionActivity_;
import com.devdn.ctc.hunterist.ui.activity.MapTripsActivity_;
import com.devdn.ctc.hunterist.ui.activity.TripsDetailActivity_;

/**
 *  Created by cuongnv on 11/11/15.
 */
public class ViewProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /**
     * This enum defines type view.
     */
    private enum Type {
        TYPE_HEADER(0), TYPE_ITEM(1);

        private final int value;

        Type(int val) {
            this.value = val;
        }

        public int getValue() {
            return value;
        }
    }

    private Context mContext;

    public ViewProfileAdapter(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == Type.TYPE_ITEM.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.item_view_trip_profile, parent, false);
            return new ItemHolder(view);
        } else if (viewType == Type.TYPE_HEADER.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.item_view_profile, parent, false);
            return new HeaderHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemHolder holder = (ItemHolder) viewHolder;
        }
    }

    @Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return Type.TYPE_HEADER.getValue();
        }
        return Type.TYPE_ITEM.getValue();
    }

    public int getBasicItemCount() {
        return 10;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     * A HeaderHolder describes an item view and metadata about its place within the RecyclerView.
     */
    class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View containerTrips;
        private View containerPlaces;
        private View containerConnections;
        private TextView mTvSumTrips;
        private TextView mTvSumConnections;
        private TextView mTvSumPlaces;
        private TextView mTvIntroduction;

        public HeaderHolder(View view) {
            super(view);
            containerTrips = (View) itemView.findViewById(R.id.containerTrips);
            containerConnections = (View) itemView.findViewById(R.id.containerConnections);
            containerPlaces = (View) itemView.findViewById(R.id.containerPlaces);
            mTvSumTrips = (TextView) itemView.findViewById(R.id.mTvSumTrips);
            mTvSumConnections = (TextView) itemView.findViewById(R.id.mTvSumConnections);
            mTvSumPlaces = (TextView) itemView.findViewById(R.id.mTvSumPlaces);
            mTvIntroduction = (TextView) itemView.findViewById(R.id.mTvIntroduction);

            containerPlaces.setOnClickListener(this);
            containerTrips.setOnClickListener(this);
            containerConnections.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                // TODO update functions cuongnv
                case R.id.containerTrips:
                    // TODO
                    break;
                case R.id.containerConnections:
                    ConnectionActivity_.intent(mContext).start();
                    break;
                case R.id.containerPlaces:
                    // TODO map
                    MapTripsActivity_.intent(mContext).start();
                    break;
                default:
                    break;
            }
        }
    }

    /**
     * A ItemHolder describes an item view bottom of the RecyclerView.
     */
    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View containerProfileTrips;

        public ItemHolder(View itemView) {
            super(itemView);
            containerProfileTrips = (View) itemView.findViewById(R.id.containerProfileTrips);
            containerProfileTrips.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.containerProfileTrips:
                    TripsDetailActivity_.intent(mContext).start();
                    break;
                default:
                    break;
            }
        }
    }
}
