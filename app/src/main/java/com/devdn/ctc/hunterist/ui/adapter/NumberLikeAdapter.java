package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.devdn.ctc.hunterist.R;

/**
 *  Created by chien on 19/11/2015.
 */
public class NumberLikeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context mContext;
    private static boolean isAdded;

    public NumberLikeAdapter(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        final View view = LayoutInflater.from(context).inflate(R.layout.item_list_like_number, parent, false);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemHolder holder = (ItemHolder) viewHolder;
        }
    }

    @Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }

    public int getBasicItemCount() {
        return 10;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    /**
     * A HeaderHolder describes an item view and metadata about its place within the RecyclerView.
     */
    class HeaderHolder extends RecyclerView.ViewHolder {
        public HeaderHolder(View view) {
            super(view);
        }
    }

    /**
     * A ItemHolder describes an item view bottom of the RecyclerView.
     */
    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private View containerAddFriend;
        private ImageView imgAddFriend;
        private ImageView mImgAvatar;
        private TextView mTvNameFriend;
        private TextView mTvMutualFriends;

        public ItemHolder(View itemView) {
            super(itemView);
            mImgAvatar = (ImageView) itemView.findViewById(R.id.imgAvatar);
            imgAddFriend = (ImageView) itemView.findViewById(R.id.imgAddFriend);
            mTvNameFriend = (TextView) itemView.findViewById(R.id.tvNameFriend);
            mTvMutualFriends = (TextView) itemView.findViewById(R.id.tvMutualFriends);
            containerAddFriend = (View) itemView.findViewById(R.id.containerAddFriend);
            containerAddFriend.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.containerAddFriend:
                    isAdded = !isAdded;
                    imgAddFriend.setSelected(isAdded);
                    break;
                default:
                    break;
            }
        }
    }
}
