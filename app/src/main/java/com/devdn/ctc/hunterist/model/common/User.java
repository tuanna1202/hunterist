package com.devdn.ctc.hunterist.model.common;

import com.google.gson.annotations.Expose;

import lombok.Data;

/**
 * Project HunterIst.
 * Copyright © 2015.
 * Created by tuanna.
 */
@Data
public class User {
    @Expose
    private String name;
}
