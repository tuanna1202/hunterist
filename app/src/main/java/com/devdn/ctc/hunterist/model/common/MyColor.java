package com.devdn.ctc.hunterist.model.common;

/**
 * Created by cuongnv on 08/11/15.
 */
public final class MyColor {
    public static final int MAIN_COLOR = 0xFF04AFEF;
    public static final int WHITE = 0xFFFFFFFF;
    public static final int BLACK = 0xff535353;
    public static final int GRAY = 0xffEDF1F2;

    private MyColor() {
        // no instance
    }
}
