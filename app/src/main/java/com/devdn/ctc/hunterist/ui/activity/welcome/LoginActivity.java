package com.devdn.ctc.hunterist.ui.activity.welcome;

import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.activity.BaseActivity;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by cuongnv on 06/11/15.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends BaseActivity {
    @ViewById
    TextInputLayout inputLayoutEmail;
    @ViewById
    TextInputLayout inputLayoutPassword;
    @ViewById
    EditText mEdtEmail;
    @ViewById
    EditText mEdtPassword;
    @ViewById
    ImageView mImgEmail;
    @ViewById
    ImageView mImgPassword;

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Override
    protected void init() {
        setEvent();
    }

    private void setEvent() {
        mEdtEmail.setOnFocusChangeListener(new MyTextFocusChange());
        mEdtPassword.setOnFocusChangeListener(new MyTextFocusChange());
        /*Set text change*/
        mEdtEmail.addTextChangedListener(new MyTextWatcher(mEdtEmail));
        mEdtPassword.addTextChangedListener(new MyTextWatcher(mEdtPassword));
    }

    @Click(R.id.mImgExit)
    void onClickExit() {
        finish();
    }

    @Click(R.id.mImgDone)
    void onClickDone() {
        submitForm();
    }

    @Click(R.id.tvRegister)
    void onClickRegister() {
        RegisterActivity_.intent(LoginActivity.this).start();
        finish();
    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateEmail()) {
            return;
        }
        if (!validatePassword()) {
            return;
        }
        Toast.makeText(getApplicationContext(), "Thank You!", Toast.LENGTH_SHORT).show();
    }

    private boolean validateEmail() {
        String email = mEdtEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(mEdtEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validatePassword() {
        if (mEdtPassword.getText().toString().trim().isEmpty()) {
            inputLayoutPassword.setError(getString(R.string.err_msg_password));
            requestFocus(mEdtPassword);
            return false;
        } else {
            inputLayoutPassword.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.mEdtEmail:
                    validateEmail();
                    break;
                case R.id.mEdtPassword:
                    validatePassword();
                    break;
            }
        }
    }

    private class MyTextFocusChange implements View.OnFocusChangeListener {
        @Override
        public void onFocusChange(View view, boolean isFocus) {
            switch (view.getId()) {
                case R.id.mEdtEmail:
                    mImgEmail.setImageResource(isFocus ? R.drawable.ic_email_press : R.drawable.ic_email);
                    break;
                case R.id.mEdtPassword:
                    mImgPassword.setImageResource(isFocus ? R.drawable.ic_pass_press : R.drawable.ic_pass);
                    break;
            }
        }
    }
}
