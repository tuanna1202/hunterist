package com.devdn.ctc.hunterist.dialog;


import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.devdn.ctc.hunterist.R;

/**
 * A class uses to show dialog choose photo when user change avatar or cover.
 *
 * @author CuongNV
 */
public class GalleryDialog extends Dialog implements View.OnClickListener {
    /**
     * Interface to handle the dialog click back to the Activity.
     */
    public interface OnButtonClickListener {
        void onCameraButtonClick();

        void onGalleryButtonClick();
    }

    private OnButtonClickListener mListener;

    public GalleryDialog(Context context, int themeResId, OnButtonClickListener l) {
        super(context, themeResId);
        mListener = l;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_gallery);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(this.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.BOTTOM;
        this.getWindow().setAttributes(layoutParams);
        this.getWindow().setWindowAnimations(R.style.GalleryDialogAnimation);
        this.setCanceledOnTouchOutside(false);

        TextView mTvOpenGallery = (TextView) findViewById(R.id.mTvOpenGallery);
        TextView mTvOpenCamera = (TextView) findViewById(R.id.mTvOpenCamera);
        TextView mTvCancel = (TextView) findViewById(R.id.mTvCancel);

        mTvOpenGallery.setOnClickListener(this);
        mTvOpenCamera.setOnClickListener(this);
        mTvCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.mTvOpenGallery:
                dismiss();
                if (mListener != null) {
                    mListener.onGalleryButtonClick();
                }
                break;
            case R.id.mTvOpenCamera:
                dismiss();
                if (mListener != null) {
                    mListener.onCameraButtonClick();
                }
                break;
            case R.id.mTvCancel:
                dismiss();
                break;
        }
    }
}
