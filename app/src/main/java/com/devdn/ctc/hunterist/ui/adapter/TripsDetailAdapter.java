package com.devdn.ctc.hunterist.ui.adapter;

import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ImageView;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.utils.animationviewpager.FixedSpeedScroller;
import com.devdn.ctc.hunterist.utils.animationviewpager.views.CustomMovingImageView;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Field;
import java.util.Timer;
import java.util.TimerTask;

/**
 *  Created by cuongnv on 11/11/15.
 */
public class TripsDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * This enum defines type view.
     */
    private enum Type {
        TYPE_HEADER(0), TYPE_ITEM(1);

        private final int value;

        Type(int val) {
            this.value = val;
        }

        public int getValue() {
            return value;
        }
    }

    private Context mContext;
    private boolean isChecked;
    private boolean isCheckedMenu;

    private CustomPagerAdapter mCustomPagerAdapter;
    private int currentPage = 0;
    private Handler handler;
    private Runnable Update;
    int[] mResources = {
            R.drawable.first,
            R.drawable.second,
            R.drawable.third,
            R.drawable.four
    };

    public TripsDetailAdapter(@NonNull Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        if (viewType == Type.TYPE_ITEM.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.item_view_trip_detail, parent, false);
            return new ItemHolder(view);
        } else if (viewType == Type.TYPE_HEADER.getValue()) {
            final View view = LayoutInflater.from(context).inflate(R.layout.content_main, parent, false);
            return new HeaderHolder(view);
        }
        throw new RuntimeException("There is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (!isPositionHeader(position)) {
            ItemHolder holder = (ItemHolder) viewHolder;
            String s1 = "https://d28vlf87tojvef.cloudfront.net/150/201401406611537127/bb3/34e/9e64386460a5e13df869397b01e1d937.jpg";
            String s2 = "https://d28vlf87tojvef.cloudfront.net/910/201401406611524581/bb3/34e/4689c91f41ee41123cb2b80950bd0cad.jpg";
            if (position % 2 == 0)
                Picasso.with(mContext).load(s1).into(holder.mImgTipPhoto);
            else
                Picasso.with(mContext).load(s2).into(holder.mImgTipPhoto);
        }
    }

    @Override
    public int getItemCount() {
        return getBasicItemCount() + 1; // header
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return Type.TYPE_HEADER.getValue();
        }
        return Type.TYPE_ITEM.getValue();
    }

    public int getBasicItemCount() {
        return 10;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private void getPopup() {
    }

    /**
     * A Item Holder describes an item view and metadata about its place within the RecyclerView.
     */
    class HeaderHolder extends RecyclerView.ViewHolder {
        private ViewPager myPager;
        public HeaderHolder(View view) {
            super(view);
            myPager = (ViewPager) view.findViewById(R.id.myPager);
            fixDuration(myPager);
            mCustomPagerAdapter = new CustomPagerAdapter(mContext);
            myPager.setAdapter(mCustomPagerAdapter);
            myPager.setPageTransformer(true, new BackgroundToForegroundTransformer());

            handler = new Handler();
            Update = new Runnable() {
                public void run() {
                    if (currentPage < 4) {
                        myPager.setCurrentItem(currentPage++, true);
                    } else {
                        currentPage = 0;
                    }
                }
            };

            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 500, 10000);
        }
    }

    private void fixDuration(ViewPager viewPager) {
        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            Interpolator sInterpolator = new AccelerateInterpolator();
            FixedSpeedScroller scroller = new FixedSpeedScroller(viewPager.getContext(), sInterpolator, false);
            mScroller.set(viewPager, scroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
    }

    /**
     * CustomPager Adapter to display image view animation
     */
    class CustomPagerAdapter extends PagerAdapter {
        Context mContext;
        LayoutInflater mLayoutInflater;

        public CustomPagerAdapter(Context context) {
            mContext = context;
            mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mResources.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

            final CustomMovingImageView imageView = (CustomMovingImageView) itemView.findViewById(R.id.img_slide);
            imageView.setImageResource(mResources[position]);
            container.addView(itemView);
            if (position == 3) {
                new CountDownTimer(25000, 1000) {
                    public void onFinish() {
                        // When timer is finished
                        // Execute your code here
                        imageView.pause();
                    }

                    public void onTick(long millisUntilFinished) {
                        // millisUntilFinished    The amount of time until finished.
                    }
                }.start();
            }
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }

    // add animation for pager when change item
    public class BackgroundToForegroundTransformer implements ViewPager.PageTransformer {

        public void transformPage(View view, float position) {
            view.setTranslationX(view.getWidth() * -position);

            if (position <= -1.0F || position >= 1.0F) {
                view.setAlpha(0.0F);
            } else if (position == 0.0F) {
                view.setAlpha(1.0F);
            } else {
                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                view.setAlpha(1.0F - Math.abs(position));
            }
        }
    }

    /**
     * A ItemHolder describes an item view bottom of the RecyclerView.
     */
    class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView mImgLikes;
        private ImageView mImgMenuPopup;
        private ImageView mImgTipPhoto;
        private View view;

        public ItemHolder(View itemView) {
            super(itemView);
            view = itemView;
            getPopup();
            mImgLikes = (ImageView) itemView.findViewById(R.id.mImgLikes);
            mImgMenuPopup = (ImageView) itemView.findViewById(R.id.mImgMenuPopup);
            mImgTipPhoto = (ImageView) itemView.findViewById(R.id.mImgTipPhoto);
            mImgLikes.setOnClickListener(this);
            mImgMenuPopup.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.mImgLikes:
                    isChecked = !isChecked;
                    mImgLikes.setSelected(isChecked);
                    break;
                case R.id.mImgMenuPopup:
                    isCheckedMenu = !isCheckedMenu;
                    mImgMenuPopup.setSelected(isCheckedMenu);
                    break;
                default:
                    break;
            }
        }
    }
}
