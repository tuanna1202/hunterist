package com.devdn.ctc.hunterist.ui.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.activity.welcome.RegisterActivity_;
import com.devdn.ctc.hunterist.ui.adapter.MainAdapter;
import com.devdn.ctc.hunterist.utils.widget.HeaderBar;
import com.devdn.ctc.hunterist.utils.widget.HidingScrollListener;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity {
    /**
     * Main Adapter
     */
    private MainAdapter mAdapter;
    @ViewById
    RecyclerView mRecyclerViewMain;
    @ViewById
    HeaderBar mHeaderBar;

    @Override
    protected void init() {
        mAdapter = new MainAdapter(this);
        mRecyclerViewMain.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewMain.setHasFixedSize(true);
        mRecyclerViewMain.setAdapter(mAdapter);
        mRecyclerViewMain.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
    }

    @Click(R.id.mBtnFloatingHome)
    void onFloatingHomeClick() {
        Toast.makeText(this, "create new trip", Toast.LENGTH_SHORT).show();
    }

    private void hideViews() {
        mHeaderBar.animate().translationY(-mHeaderBar.getHeight()).setInterpolator(new AccelerateInterpolator(2));
    }

    private void showViews() {
        mHeaderBar.animate().translationY(0).setInterpolator(new DecelerateInterpolator(2));
    }

}
