package com.devdn.ctc.hunterist.ui.activity;

import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.devdn.ctc.hunterist.R;
import com.devdn.ctc.hunterist.ui.adapter.TripsDetailAdapter;
import com.devdn.ctc.hunterist.utils.widget.HeaderBar;
import com.devdn.ctc.hunterist.utils.widget.HidingScrollListener;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

/**
 * Created by cuongnv on 17/11/15.
 */
@EActivity(R.layout.activity_trips_detail)
public class TripsDetailActivity extends BaseActivity {
    /**
     * Trip Detail Adapter
     */
    private TripsDetailAdapter mAdapter;
    @ViewById
    RecyclerView mRecyclerViewMain;
    @ViewById
    HeaderBar mHeaderBar;

    @Override
    protected void init() {
        mAdapter = new TripsDetailAdapter(this);
        mRecyclerViewMain.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerViewMain.setHasFixedSize(true);
        mRecyclerViewMain.setAdapter(mAdapter);
        mRecyclerViewMain.addOnScrollListener(new HidingScrollListener() {
            @Override
            public void onHide() {
                hideViews();
            }

            @Override
            public void onShow() {
                showViews();
            }
        });
        mHeaderBar.setOnHeaderBarListener(new HeaderBar.MyOnHeaderBarListener() {
            @Override
            public void onLeftButtonClick(View v) {
                onBackPressed();
            }

            @Override
            public void onRightButtonClick(View v) {
                MapTripsActivity_.intent(TripsDetailActivity.this).start();
            }
        });
    }

    @Click(R.id.mBtnFloatingHome)
    void onFloatingHomeClick() {
        Toast.makeText(this, "create new trip", Toast.LENGTH_SHORT).show();
    }

    private void hideViews() {
        mHeaderBar.setTitle("CuongNV");
        mHeaderBar.setTitleTextColor(Color.WHITE);
    }

    private void showViews() {
        mHeaderBar.setTitle("");
    }
}
