package com.devdn.ctc.hunterist.model;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

/**
 * Class Object ImageOnPhone.
 *
 * @author CuongNV
 */
@Data
public class ImageOnPhone implements Parcelable {

    public static final Creator<ImageOnPhone> CREATOR = new Creator<ImageOnPhone>() {
        public ImageOnPhone createFromParcel(Parcel source) {
            return new ImageOnPhone(source);
        }

        public ImageOnPhone[] newArray(int size) {
            return new ImageOnPhone[size];
        }
    };

    private int id;
    private String path;

    public ImageOnPhone() {
    }

    protected ImageOnPhone(Parcel in) {
        this.id = in.readInt();
        this.path = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.path);
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
